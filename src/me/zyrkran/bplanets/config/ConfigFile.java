package me.zyrkran.bplanets.config;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import com.google.common.io.ByteStreams;

import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

public class ConfigFile {
	
	Plugin plugin;
	
	File file;
	String filename;
	Configuration config;
	
	public ConfigFile(Plugin plugin, String filename) {
		this.plugin = plugin;
		this.filename = filename;
		this.file = new File(plugin.getDataFolder(), filename);

		if (!plugin.getDataFolder().exists()) {
			plugin.getDataFolder().mkdir();
		}
		
		if (!file.exists()) {
			try (InputStream in = plugin.getResourceAsStream(filename)){
				OutputStream out = new FileOutputStream(file);
				ByteStreams.copy(in, out);
			} catch (IOException e) {	}	
		}

		try {
			config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
		} catch (IOException e) { 	}
	}
	
	public Configuration getConfig() {
		return config;
	}
	
	public void saveConfig() {
		try {
			ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, new File(plugin.getDataFolder(), filename));
		} catch (IOException e) { }
	}

}
