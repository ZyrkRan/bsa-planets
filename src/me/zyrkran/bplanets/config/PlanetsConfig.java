package me.zyrkran.bplanets.config;

import java.util.List;

import net.md_5.bungee.api.plugin.Plugin;

public class PlanetsConfig extends ConfigFile {

	public PlanetsConfig(Plugin plugin, String filename) {
		super(plugin, filename);
	}
	
	public void setLastRestore(String planet, String data) {
		getConfig().set("planets." + planet + ".last-restore", data);
	}
	
	public String getLastRestore(String planet) {
		return getConfig().getString("planets." + planet + ".last-restore");
	}
	
	public boolean isRestorable(String planet) {
		return getConfig().getBoolean("planets." + planet + ".restorable");
	}

	public int getPlayerLimit(String planet) {
		return getConfig().getInt("planets." + planet + ".player-limit");
	}
	
	public String getServer(String planet) {
		return getConfig().getString("planets." + planet + ".server");
	}
	
	public List<String> getPlanets(){
		return getConfig().getStringList("planets");
	}

}
