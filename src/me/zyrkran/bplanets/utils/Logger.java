package me.zyrkran.bplanets.utils;

public class Logger {
	
	static boolean activated = true;

	public static void log(Object string) {
		if (activated) {
			System.out.println(string);
		}
	}
}
