package me.zyrkran.bplanets;

import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PlanetManager {
	
	private static List<Planet> planets;
	
	public static void connectPlayer(ProxiedPlayer player, String planetName) {
		Planet planet = Planet.byName(planetName);
		if (planet != null) {
			ServerInfo info = BungeeCord.getInstance().getServerInfo(planet.getServer());
			player.connect(info);
		}
	}
	
	public static void playerJoinPlanet(ProxiedPlayer player, String planetName) {
		Planet planet = Planet.byName(planetName);
		
		// check if player is already in another planet
		for (Planet p : getPlanets()) {
			if (p.getPlayers().contains(player)) {
				p.removePlayer(player);
			}
		}
		
		planet.addPlayer(player);
	}

	public static void playerLeavePlanet(ProxiedPlayer player, String planet) {
		Planet p = Planet.byName(planet);		
		p.removePlayer(player);
	}
	
	public static int getPlayerCount(String planetName) {
		Planet planet = Planet.byName(planetName);
		if (planet == null) {
			return 0;
		}
		return planet.getPlayers().size();
	}
	
	public static Planet getPlanetByPlayer(ProxiedPlayer player) {
		for (Planet planet : getPlanets()) {
			if (planet.getPlayers().contains(player)) {
				return planet;
			}
		}
		
		return null;
	}
	
	public static void loadPlanets() {
		planets = new ArrayList<Planet>();
		for (String s : Main.getPlanetsConfig().getConfig().getSection("planets").getKeys()) {
			String server;
			int player_limit;
			boolean restorable;
			
			server = Main.getPlanetsConfig().getServer(s);
			restorable = Main.getPlanetsConfig().isRestorable(s);
			player_limit = Main.getPlanetsConfig().getPlayerLimit(s);
					
			planets.add(new Planet(s, server, restorable, player_limit));
		}
	}
	
	public static List<Planet> getPlanets(){
		return planets;
	}
}
