package me.zyrkran.bplanets;

import java.util.HashMap;

import fr.rhaz.socket4mc.Bungee.BungeeSocketDisconnectEvent;
import fr.rhaz.socket4mc.Bungee.BungeeSocketHandshakeEvent;
import fr.rhaz.socketapi.SocketAPI.Server.SocketMessenger;
import me.zyrkran.bplanets.Planet.PlanetState;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class SocketManager implements Listener{
	
	private static HashMap<String, SocketMessenger> sockets;
	
	public SocketManager() {
		sockets = new HashMap<>();
	}
	
	public static void sendData(String server, String channel, String data) {
		if (!sockets.containsKey(server)) {
			return;
		}
		sockets.get(server).writeJSON(channel, data);
	}
	
	public static void addSocketMessenger(String server, SocketMessenger messenger) {
		sockets.put(server, messenger);
		
//		// send servers information about the last time planets were restored
//		sendData(server, "SA_Planets", "RESTORE_LAST;" + server + ";" + Main.getPlanetsConfig().getLastRestore(server));
		
	}
	
	public static void removeSocket(String server) {
		sockets.remove(server);
	}
	
	public static boolean isOnline(String planetName) {
		if (!sockets.containsKey(planetName)) {
			return false;
		}
		
		return sockets.get(planetName).isHandshaked() && sockets.get(planetName).isConnectedAndOpened();
	}
	
	public static HashMap<String, SocketMessenger> getSockets(){
		return sockets;
	}
	
	@EventHandler
	public void onServerDisconnect(BungeeSocketDisconnectEvent event) {
		Planet planet = Planet.byName(event.getMessenger().getName());
		if (planet == null) {
			return;
		}

		// stop communication with server
		removeSocket(planet.getName()); 
		
		if (planet.isRestorable()) {
			planet.stopRestoreCount();
			planet.setState(PlanetState.CLOSED);
		}
	}
	
	@EventHandler
	public void onHandShake(BungeeSocketHandshakeEvent event) {	
		SocketManager.addSocketMessenger(event.getName(), event.getMessenger());
		
		System.out.println("Connected: " + event.getMessenger().getName());
		
		Planet planet = Planet.byName(event.getMessenger().getName());
		if (planet == null) {
			return;
		}

		planet.setState(PlanetState.OPEN);
		
		if (planet.isRestorable()) {
			planet.startRestoreCount();
		}
	}
}