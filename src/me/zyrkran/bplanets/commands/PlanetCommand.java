package me.zyrkran.bplanets.commands;

import me.zyrkran.bplanets.Main;
import me.zyrkran.bplanets.Planet;
import me.zyrkran.bplanets.PlanetManager;
import me.zyrkran.bplanets.SocketManager;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class PlanetCommand extends Command {

	public PlanetCommand() {
		super("planets");
	}
	
	/*
	 * planets.join
	 * planets.leave
	 * planets.list
	 * planets.restore
	 * 
	 */

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (!(sender instanceof ProxiedPlayer)) {
			return;
		}
		
		ProxiedPlayer player = (ProxiedPlayer) sender;
		
		if (args.length == 0) {
			Planet planet = PlanetManager.getPlanetByPlayer(player);
			if (planet == null) {
				sendMessage(player, ChatColor.RED + "Couldn't find that planet!");
				return;
			}
			
			SocketManager.sendData(planet.getName(), "SA_Planets", "OPEN_NAVIGATOR;" + player.getUniqueId());
			return;
		}
		
		else if (args[0].equalsIgnoreCase("help")) {
			sendMessage(player, " ");
			sendMessage(player, "/planets help - Display this guide.");
			sendMessage(player, "/planets join <planet> - Join a planet.");
			sendMessage(player, "/planets leave - Return to lobby.");
			sendMessage(player, "/planets list - List all planets.");
			sendMessage(player, "/planets restore <planet> - Restore a planet.");
			sendMessage(player, " ");
			return;
		}
		
		else if (args[0].equalsIgnoreCase("list")) {
			if (player.hasPermission("planets.list")) {
				StringBuilder sb = new StringBuilder();
				for (Planet planet : PlanetManager.getPlanets()) {
					boolean online = SocketManager.isOnline(planet.getName());
					sb.append((online ? ChatColor.GREEN : ChatColor.RED.toString() + ChatColor.STRIKETHROUGH) + planet.getName() + ChatColor.GOLD + ", ");
				}
				
				sb.setCharAt(sb.length()-2, ' ');
				sendMessage(player, ChatColor.GOLD + "Available planets: " + sb.toString().trim());
				return;
			} else {
				sendMessage(player, ChatColor.RED + "You don't have permission to do that!");
				return;
			}
		}
		
		else if (args[0].equalsIgnoreCase("leave")) {
			if (player.hasPermission("planets.leave")) {
				player.connect(Main.getLobbyServer());
				sendMessage(player, ChatColor.GOLD + "Sending you back to lobby...");
				return;
			} else {
				sendMessage(player, ChatColor.RED + "You don't have permission to do that!");
				return;
			}			
		}
		
		else if (args[0].equalsIgnoreCase("join")) {
			if (player.hasPermission("planets.join")) {
				if (args.length != 2) {
				sendMessage(player, ChatColor.RED + "Usage: /planets join <planet>");
				return;
				}
			
				Planet planet = Planet.byName(args[1]);
				if (planet == null) {
					sendMessage(player, ChatColor.RED + "Error: Invalid planet name!");
					return;
				}
				
				if (!SocketManager.isOnline(planet.getName())) {
					sendMessage(player, ChatColor.RED + "Error: Planet is offline!");
					return;
				}
				
				if (player.getServer().getInfo().getName().equalsIgnoreCase(planet.getName())) {
					sendMessage(player, ChatColor.RED + "You are already connected to this planet!");
					return;
				}
				
				// join planet
				PlanetManager.connectPlayer(player, planet.getName());
				sendMessage(player, ChatColor.GOLD + "Connecting to " + ChatColor.GREEN + planet.getName() + ChatColor.GOLD + "...");
	
			} else {
				sendMessage(player, ChatColor.RED + "You don't have permission to do that!");
				return;
			}
			
		}
		
		else if (args[0].equalsIgnoreCase("restore")) {
//			if (player.hasPermission("planets.restore")) {
				Planet planet = Planet.byName("planet1");
				planet.restore();
				sendMessage(player, ChatColor.GOLD + "Attempted to restore planet!");
				return;
//			} else {
//				sendMessage(player, ChatColor.RED + "You don't have permission to do that!");
//				return;
//			}
			
		}
		
		// unknown command
		else {
			sendMessage(player, ChatColor.GOLD + "For help type /planets help");
			return;
		}
	}
	
	private void sendMessage(ProxiedPlayer player, String msg) {
		player.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', msg)));
	}
}
