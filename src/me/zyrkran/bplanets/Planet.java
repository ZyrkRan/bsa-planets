package me.zyrkran.bplanets;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.scheduler.ScheduledTask;

public class Planet {
	
	// Check if player limit is full and return boolean

	private ScheduledTask task;
	private PlanetState state;
	private List<ProxiedPlayer> players;
	
	private int player_limit;
	private boolean restorable;
	
	private String name;
	private String server;
	
	public Planet(String name, String server, boolean restorable, int player_limit) {
		this.name = name;
		this.server = server;
		this.restorable = restorable;
		this.player_limit = player_limit;
		this.players = new ArrayList<>();
		this.state = PlanetState.CLOSED;
		
		/* SEND PLANET INFORMATION TO SPIGOT */
		BungeeCord.getInstance().getScheduler().schedule(Main.getInstance(), new Runnable() {
			@Override
			public void run() {
				for (String s : SocketManager.getSockets().keySet()) {
					SocketManager.sendData(s, "SA_Planets", "PLAYER_COUNT;" + name + ";" + getPlayers().size());
					SocketManager.sendData(s, "SA_Planets", "PLANET_STATUS;" + name + ";" + SocketManager.isOnline(name));
					SocketManager.sendData(s, "SA_Planets", "PLANET_STATE;" + name + ";" + getState());
				}
			}
		}, 0, 2, TimeUnit.SECONDS);
	}
	
	public String getName() {
		return name;
	}
	
	public String getServer() {
		return server;
	}
	
	public void addPlayer(ProxiedPlayer player) {
		players.add(player);
	}
	
	public void removePlayer(ProxiedPlayer player) {
		players.remove(player);
	}
	
	public int getPlayerLimit() {
		return player_limit;
	}
	
	public List<ProxiedPlayer> getPlayers(){
		return players;
	}
	
	public void setState(PlanetState state) {
		this.state = state;
	}
	
	public PlanetState getState() {
		return state;
	}
	
	public boolean isRestorable() {
		return restorable;
	}
	
	public void stopRestoreCount() {
		if (task != null) {
			task.cancel();
		}
	}
	
	public void startRestoreCount() {
		task = BungeeCord.getInstance().getScheduler().schedule(Main.getInstance(), new Runnable() {
			
			int timeleft = 60;
			
			@Override
			public void run() {
				if (timeleft <= 15) {
					if (timeleft <= 0) {
						restore();
						return;
					}
					
					for (ProxiedPlayer player : BungeeCord.getInstance().getServerInfo(server).getPlayers()) {
						player.sendMessage(new TextComponent(ChatColor.GOLD + "Planet is restoring in " + timeleft + " seconds..."));
					}
				}

				timeleft--;
				BungeeCord.getInstance().broadcast("DEBUG: (" + server + " restoring in " + timeleft + "...)");
			}
			
		}, 0, 1, TimeUnit.SECONDS);
	}
	
	public void restore() {
		for (ProxiedPlayer player : players) {
			PlanetManager.connectPlayer(player, "lobby");
			
			// notify
			player.sendMessage(new TextComponent(ChatColor.GOLD + "Planet is being restored..."));
		}
		
		SocketManager.sendData(server, "SA_Planets", "RESTORE_PLANET;" + name);
	}
	
	public static Planet byServer(String server) {
		for (Planet planet : PlanetManager.getPlanets()) {
			if (planet.getServer().equalsIgnoreCase(server)) {
				return planet;
			}
		}
		return null;
	}
	
	public static Planet byName(String name) {
		for (Planet planet : PlanetManager.getPlanets()) {
			if (planet.getName().equalsIgnoreCase(name)) {
				return planet;
			}
		}
		return null;
	}
	
	public enum PlanetState{
		OPEN, RESTORING, CLOSED;
	}
}

