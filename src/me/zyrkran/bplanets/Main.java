package me.zyrkran.bplanets;

import me.zyrkran.bplanets.commands.PlanetCommand;
import me.zyrkran.bplanets.config.PlanetsConfig;
import me.zyrkran.bplanets.listeners.PlayerListener;
import me.zyrkran.bplanets.listeners.ServerDataListener;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;

public class Main extends Plugin {
	
	private static Main instance;
	private static PlanetsConfig config;
	
	private SocketManager socketManager;
	
	public void onEnable() {
		instance = this;
		config = new PlanetsConfig(this, "config.yml");
		socketManager = new SocketManager();
		
		// Register command
		BungeeCord.getInstance().getPluginManager().registerCommand(this, new PlanetCommand());
		
		// Register listener
		BungeeCord.getInstance().getPluginManager().registerListener(this, new SocketManager());
		BungeeCord.getInstance().getPluginManager().registerListener(this, new PlayerListener());
		BungeeCord.getInstance().getPluginManager().registerListener(this, new ServerDataListener());

		// load all planets
		PlanetManager.loadPlanets(); 
	}
	
	public SocketManager getSocketManager() {
		return socketManager;
	}
	
	public static Main getInstance() {
		return instance;
	}
	
	public static PlanetsConfig getPlanetsConfig() {
		return config;
	}
	
	public static ServerInfo getLobbyServer() {
		return BungeeCord.getInstance().getServerInfo(config.getConfig().getString("planets.lobby.server"));
	}
}
