package me.zyrkran.bplanets.listeners;

import me.zyrkran.bplanets.Planet;
import me.zyrkran.bplanets.PlanetManager;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class PlayerListener implements Listener{
	
	@EventHandler
	public void onConnect(ServerSwitchEvent event) {
		ProxiedPlayer player = event.getPlayer();
		Planet planet = Planet.byServer(event.getPlayer().getServer().getInfo().getName());
		
		if (planet != null) {
			PlanetManager.playerJoinPlanet(player, planet.getName());
			System.out.println("Player added to " + planet.getName() + "(" + planet.getServer() + ")");
			return;
		}
	}
	
	@EventHandler
	public void onDisconnect(PlayerDisconnectEvent event) {
		ProxiedPlayer player = event.getPlayer();
		Planet planet = Planet.byServer(event.getPlayer().getServer().getInfo().getName());
		
		if (planet != null) {
			PlanetManager.playerLeavePlanet(player, planet.getName());
			return;
		}
		
		
	}

}
