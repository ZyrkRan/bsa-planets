package me.zyrkran.bplanets.listeners;

import java.util.UUID;

import fr.rhaz.socket4mc.Bungee.BungeeSocketJSONEvent;
import me.zyrkran.bplanets.Planet;
import me.zyrkran.bplanets.PlanetManager;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class ServerDataListener implements Listener{
	
	@EventHandler /* This reads data coming from SPIGOT */
	public void onSocketJSONMessage(BungeeSocketJSONEvent event) {
		String channel 	= event.getChannel();
		
		System.out.println(channel + " / " + event.getData());
		
		if (channel.equalsIgnoreCase("SA_Planets")) {
			String command = event.getData().split(";")[0];
			String[] data 	= event.getData().split(";");
			
			// CONNECT;PLAYER_UUID;SERVER_NAME
			if (command.equalsIgnoreCase("CONNECT")) { 
				ProxiedPlayer player = BungeeCord.getInstance().getPlayer(UUID.fromString(data[1]));
				
				Planet planet = Planet.byName(data[2]);
				if (planet ==  null) {
					System.out.println("Couldn't connect to " + data[2]);
					return;
				}
			
				PlanetManager.connectPlayer(player, planet.getName());
			}
			
			// RESTORE_PLANET_CONFIRM;SERVER_NAME
			else if (command.equalsIgnoreCase("RESTORE_PLANET_CONFIRM")) {
				Planet planet = Planet.byName(data[1]);
				if (planet == null) {
					return;
				}
				
//				if (planet.getState() == PlanetState.RESTORING) {
//					Date date = Calendar.getInstance().getTime();
//					SimpleDateFormat format = new SimpleDateFormat("dd/MMM/yyyy@hh:mm");
//					
//					//Main.getPlanetsConfig().setLastRestore(data[1], format.format(date));	
//				}
			}
		}
	}
}
